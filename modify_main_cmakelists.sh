# Modify ABI to 1, because protobuf for centos8s is compiled with ABI=1
sed -i 's/ABI=0/ABI=1/g' CMakeLists.txt

sed -e '/(xroot_plugins/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(frontend-grpc/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(catalogue/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(xroot_plugins/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(disk/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(mediachanger/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(objectstore/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(python/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(rdbms/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(scheduler/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(tapeserver/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(statistics/ s/^#*/#/' -i CMakeLists.txt
sed -e '/(continuousintegration/ s/^#*/#/' -i CMakeLists.txt
sed -e '/tory(tests/ s/^#*/#/' -i CMakeLists.txt
sed -e '/cta-release/ s/^#*/#/' -i CMakeLists.txt
