#!/bin/bash

set -ex

yum install -y gcc gcc-c++ cmake3 make rpm-build
sed -i 's/protobuf3-/protobuf-/g' cta.spec.in
sed -i 's/instantclient19.3/instantclient19.15/g' cta.spec.in
sed -i 's/19.3/19.15/g' cmake/Findoracle-instantclient.cmake
sed -i 's/protobuf3/protobuf/g' cmake/FindProtobuf3.cmake
sed -i 's/lib64\/protobuf/lib64/g' cmake/FindProtobuf3.cmake
sed -i 's/protoc3/protoc/g' cmake/FindProtobuf3.cmake
sed -i 's/include\/protobuf/include/g' cmake/FindProtobuf3.cmake
mkdir ../build_srpm
cd ../build_srpm
cmake3 -DPackageOnly:Bool=true ../CTA
make cta_srpm

cd ..

yum install -y gcc gcc-c++ cmake3 make rpm-build yum-utils
yum install -y epel-release git
yum-builddep --nogpgcheck -y build_srpm/RPM/SRPMS/*
mkdir -p ./build_rpm
cd ./build_rpm
cmake3 ../CTA -DSKIP_UNIT_TESTS:STRING=1
make cta_rpm
