FROM gitlab-registry.cern.ch/linuxsupport/cs8-base

LABEL maintainer="Jorge Camarero Vera"
LABEL maintainer_mail="jorge.camarero@cern.ch"

ENV USER="develop"

COPY ./repos/ /etc/yum.repos.d/

RUN set -ex; \
    # cryptopp-devel
    yum -y install epel-release git; \
    yum update -y;

WORKDIR /Project

RUN set -ex; \
    git clone https://gitlab.cern.ch/cta/CTA.git; \
    cd CTA; \
    git submodule update --init --recursive

COPY Findgmock.cmake /CMake/
COPY build_rpms.sh /Scripts/
COPY modify_main_cmakelists.sh /Scripts/
COPY cta.spec.in /Project/CTA/

RUN set -ex; \
    cd CTA; \
    cp /CMake/Findgmock.cmake ./cmake/; \
    chmod +x /Scripts/*.sh; \
    /Scripts/modify_main_cmakelists.sh; \
    /Scripts/build_rpms.sh;

RUN set -ex; \
    mkdir /RPM; \
    cp -R ./build_srpm/RPM/SRPMS /RPM; \
    cp -R ./build_rpm/RPM/RPMS /RPM
